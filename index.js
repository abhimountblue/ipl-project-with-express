const express = require('express')
const app = express()
require('dotenv').config()
const howManyMatchesPerYear = require('./src/server/1-matches-per-year.cjs')
const winsEveryTeamEverYear = require('./src/server/2-matches-won-per-team-per-year.cjs')
const extraRunsConcededByEveryTeamYear2016 = require('./src/server/3-extra-runs-per-team-year16.cjs')
const top10EconomicalBowlerYear2015 = require("./src/server/4-top-10-economical-bowlers-year-2015.cjs")
const teamWonTossAndMatch = require('./src/server/5-each-team-won-toss-won-match.cjs')
const mostPlayerOfTheMatchEachSeason = require('./src/server/6-most-player-of-the-match-each-season.cjs')
const betsmanWithStrikeRateEachSeason = require('./src/server/7-betsman-with-strike-rate-of-each-season.cjs')
const highestTimeOnePlayerDismissedBynotherPlayer = require('./src/server/8-highest-time-one-player-dismissed-by-another-player.cjs')
const bowlerWithBestEconomyInSuperOber = require('./src/server/9-bowler-with-best-economi-in-superover.cjs')


app.get('/matches-per-year', (req, res) => {
    const result = howManyMatchesPerYear()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)

})

app.get('/wins-every-team-every-year', (req, res) => {
    const result = winsEveryTeamEverYear()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)

})

app.get('/extra-runs-per-teams-year-2016', (req, res) => {
    const result = extraRunsConcededByEveryTeamYear2016()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)
})

app.get('/top-10-economical-bowlers-year-2015', (req, res) => {
    const result = top10EconomicalBowlerYear2015()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)
})

app.get('/each-team-won-toss-won-match', (req, res) => {
    const result = teamWonTossAndMatch()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)
})

app.get('/most-player-of-the-match-each-season', (req, res) => {
    const result = mostPlayerOfTheMatchEachSeason()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)
})

app.get('/betsman-with-strike-rate-of-each-season', (req, res) => {
    const result = betsmanWithStrikeRateEachSeason()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)
})

app.get('/highest-time-one-player-dismissed-by-another-player', (req, res) => {
    const result = highestTimeOnePlayerDismissedBynotherPlayer()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)
})

app.get('/bowler-with-best-economi-in-superover', (req, res) => {
    const result = bowlerWithBestEconomyInSuperOber()
    res.header('Content-type', 'application/json')
    res.status(200).send(result)
})

const port = process.env.PORT || 3000
app.listen(port, () => {
    console.log(`server is running on ${port} port`)
})



