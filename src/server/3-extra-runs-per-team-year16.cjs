const papa = require("papaparse")
const fs = require("fs")
const path = require("path")

const csvDeliveries = fs.readFileSync(
  path.resolve(__dirname, "../data/deliveries.csv"),
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, { header: true })
jsonDeliveries = jsonDeliveries.data

const csvMatches = fs.readFileSync(
  path.resolve(__dirname, "../data/matches.csv"),
  "utf-8"
)

let jsonMatches = papa.parse(csvMatches, { header: true })
jsonMatches = jsonMatches.data

function extraRunsConcededByEveryTeamYear2016() {
  // filtering all matches that played in 2016
  const yearOf2016Matches = jsonMatches.filter((element) => {
    return element.season === "2016"
  })
  const teamObject = yearOf2016Matches.reduce((accumulator, element) => {
    if (element.team1 !== "undefined" && element.team2 !== "undefined") {
      if (!accumulator[element["team1"]]) {
        accumulator[element["team1"]] = 0
      }
      if (!accumulator[element["team2"]]) {
        accumulator[element["team2"]] = 0
      }
    }

    const matchDeliveri = jsonDeliveries.filter((item) => {
      return item.match_id === element.id
    })

    const firstInning = matchDeliveri.filter((team) => {
      return team.inning === "1"
    })

    const secondInning = matchDeliveri.filter((team) => {
      return team.inning === "2"
    })

    // calculating first inning extra runs
    const firstInningExtraRuns = firstInning.reduce((acc, team) => {
      return Number(team.extra_runs) + acc
    }, 0)

    // calculating second inning extra runs
    const secondInningExtraRuns = secondInning.reduce((acc, team) => {
      return Number(team.extra_runs) + acc
    }, 0)
    // assigning extra runs given by teams with teams key in object
    accumulator[firstInning[0]["bowling_team"]] =
      accumulator[firstInning[0]["bowling_team"]] + firstInningExtraRuns
    accumulator[secondInning[0]["bowling_team"]] =
      accumulator[secondInning[0]["bowling_team"]] + secondInningExtraRuns
    return accumulator
  }, {})
  return teamObject
}

module.exports = extraRunsConcededByEveryTeamYear2016
