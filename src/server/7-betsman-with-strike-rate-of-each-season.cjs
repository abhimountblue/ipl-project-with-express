const papa = require("papaparse")
const fs = require("fs")
const path = require("path")

const csvDeliveries = fs.readFileSync(
  path.resolve(__dirname, "../data/deliveries.csv"),
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, { header: true })
jsonDeliveries = jsonDeliveries.data

const csvMatches = fs.readFileSync(
  path.resolve(__dirname, "../data/matches.csv"),
  "utf-8"
)

let jsonMatches = papa.parse(csvMatches, { header: true })
jsonMatches = jsonMatches.data

function betsmanWithStrikeRateEachSeason() {
  const matchIdSeason = jsonMatches.reduce((accumulator, match) => {
    accumulator[match.id] = match.season
    return accumulator
  }, {})

  jsonDeliveries = jsonDeliveries.map((delivery) => {
    return { ...delivery, season: matchIdSeason[delivery.match_id] }
  })
  const allBatsManWithStrikeRate = jsonDeliveries.reduce(
    (accumulator, element) => {
      if (
        !accumulator[element.batsman] &&
        typeof element.batsman !== "undefined"
      ) {
        const allBallPlayedByBatsman = jsonDeliveries.filter((item) => {
          return item.batsman === element.batsman
        })
        const allSeason = allBallPlayedByBatsman.reduce((acc, value) => {
          if (!acc[value.season]) {
            const seasonDeliveries = allBallPlayedByBatsman.filter((data) => {
              return value.season === data.season
            })
            const allPlayedBallCount = seasonDeliveries.reduce(
              (acc, deliverys) => {
                return acc + 1
              },
              0
            )
            const allRunsByBatsman = seasonDeliveries.reduce(
              (acc, deliverys) => {
                return acc + Number(deliverys.batsman_runs)
              },
              0
            )
            let strikeRate = (allRunsByBatsman / allPlayedBallCount) * 100
            acc[value.season] = strikeRate
          }
          return acc
        }, {})
        accumulator[element.batsman] = allSeason
      }
      return accumulator
    },
    {}
  )
  return allBatsManWithStrikeRate
}

module.exports = betsmanWithStrikeRateEachSeason
