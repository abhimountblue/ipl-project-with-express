const papa = require("papaparse")
const fs = require("fs")
const path = require("path")

const csvDeliveries = fs.readFileSync(
  path.resolve(__dirname, "../data/deliveries.csv"),
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, { header: true })
jsonDeliveries = jsonDeliveries.data

function highestTimeOnePlayerDismissedBynotherPlayer() {
  let dismissedArray = jsonDeliveries.filter((element) => {
    return element.player_dismissed !== ""
  })

  const mostDissmissedPlayer = dismissedArray.reduce((accumulator, element) => {
    if (typeof element.player_dismissed === "undefined") {
      return accumulator
    }
    if (!accumulator[element.player_dismissed]) {
      const dismissedByPlayerArray = dismissedArray.filter((item) => {
        return item.player_dismissed === element.player_dismissed
      })
      const dismissedByPlayer = dismissedByPlayerArray.reduce(
        (accumulator, value) => {
          accumulator[value.bowler] = (accumulator[value.bowler] || 0) + 1
          return accumulator
        },
        {}
      )

      const mostdismissedByPlayer = Object.keys(dismissedByPlayer).reduce(
        (accumulator, key) => {
          if (
            dismissedByPlayer[key] > accumulator.mostdismissedByPlayerNumber
          ) {
            accumulator.mostdismissedByPlayerName = key
            accumulator.mostdismissedByPlayerNumber = dismissedByPlayer[key]
          }
          return accumulator
        },
        { mostdismissedByPlayerName: "", mostdismissedByPlayerNumber: 0 }
      )
      accumulator[element.player_dismissed] = {
        [mostdismissedByPlayer.mostdismissedByPlayerName]:
          mostdismissedByPlayer.mostdismissedByPlayerNumber,
      }
    }
    return accumulator
  }, {})
  return mostDissmissedPlayer
}
module.exports = highestTimeOnePlayerDismissedBynotherPlayer
