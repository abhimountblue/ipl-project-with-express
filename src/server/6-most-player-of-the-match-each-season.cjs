const papa = require("papaparse")
const fs = require("fs")
const path = require("path")

const csvMatches = fs.readFileSync(
  path.resolve(__dirname, "../data/matches.csv"),
  "utf-8"
)

let jsonMatches = papa.parse(csvMatches, { header: true })
jsonMatches = jsonMatches.data

function mostPlayerOfTheMatchEachSeason() {
  const seasons = jsonMatches.reduce((accumulator, element) => {
    if (!accumulator[element.season] && typeof element.season !== "undefined") {
      let allMatchSeason = jsonMatches.filter((item) => {
        return item.season === element.season
      })
      const playerOfTheMatch = allMatchSeason.reduce((accumulator, item) => {
        accumulator[item.player_of_match] =
          (accumulator[item.player_of_match] || 0) + 1
        return accumulator
      }, {})
      const mostPlayerOfTheMatch = Object.keys(playerOfTheMatch).reduce(
        (accumulator, key) => {
          if (playerOfTheMatch[key] > accumulator.mostPlayerOfTheMatchNumber) {
            accumulator.mostPlayerOfTheMatchName = key
            accumulator.mostPlayerOfTheMatchNumber = playerOfTheMatch[key]
          }
          return accumulator
        },
        { mostPlayerOfTheMatchName: "", mostPlayerOfTheMatchNumber: 0 }
      )
      accumulator[element.season] = {
        [mostPlayerOfTheMatch.mostPlayerOfTheMatchName]:
          mostPlayerOfTheMatch.mostPlayerOfTheMatchNumber,
      }
    }
    return accumulator
  }, {})
  return seasons
}

module.exports = mostPlayerOfTheMatchEachSeason
// const result = mostPlayerOfTheMatchEachSeason(jsonMatches)

// fs.writeFileSync(
//   "/home/abhishek/Desktop/ipl/src/public/output/6-most-player-of-the-match-each-season.json",
//   JSON.stringify(seasons, null, 2)
// )
