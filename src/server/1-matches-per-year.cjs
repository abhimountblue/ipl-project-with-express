const path = require("path")
const papa = require("papaparse")
const fs = require("fs")


function howManyMatchesPerYear() {
  const csvMatches = fs.readFileSync(path.resolve(__dirname, '../data/matches.csv'), "utf-8")
  // converting csv file to json using papa parse
  const jsonMatches = papa.parse(csvMatches, { header: true, })
  const seasonPerMatches = jsonMatches.data.reduce((accumulator, element) => {
    if (typeof element.season === "undefined") {
      return accumulator
    }
    if (accumulator.hasOwnProperty(element.season)) {
      accumulator[element.season] = accumulator[element.season] + 1
    } else {
      accumulator[element.season] = 1
    }
    return accumulator
  }, {})
  return seasonPerMatches
}

module.exports = howManyMatchesPerYear