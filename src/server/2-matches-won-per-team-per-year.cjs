const papa = require("papaparse")
const fs = require("fs")
const path = require('path')

const csvMatches = fs.readFileSync(path.resolve(__dirname, '../data/matches.csv'), "utf-8")
// converting csv file to json using papa parse
let jsonMatches = papa.parse(csvMatches, {
  header: true,
})
jsonMatches = jsonMatches.data

// created a function it will return occurence of every teams win in every season
function winsEveryTeamEverYear() {
  const teams = jsonMatches.reduce((accumulator, element) => {
    if (typeof element.season === "undefined") {
      return accumulator
    }
    if (
      !accumulator.hasOwnProperty(element.team1) &&
      typeof element.team1 !== "undefined"
    ) {
      accumulator[element.team1] = {}
    }
    if (
      !accumulator.hasOwnProperty(element.team2) &&
      typeof element.team2 !== "undefined"
    ) {
      accumulator[element.team2] = {}
    }
    if (element.season && element.winner) {
      if (accumulator[element.winner][element.season]) {
        accumulator[element.winner][element.season] =
          accumulator[element.winner][element.season] + 1
      } else {
        accumulator[element.winner][element.season] = 1
      }
    }
    return accumulator
  }, {})
  return teams
}
module.exports = winsEveryTeamEverYear

