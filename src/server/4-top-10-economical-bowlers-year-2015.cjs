const papa = require("papaparse")
const fs = require("fs")
const path = require("path")

const csvDeliveries = fs.readFileSync(
  path.resolve(__dirname, "../data/deliveries.csv"),
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, { header: true })
jsonDeliveries = jsonDeliveries.data

const csvMatches = fs.readFileSync(
  path.resolve(__dirname, "../data/matches.csv"),
  "utf-8"
)

let jsonMatches = papa.parse(csvMatches, { header: true })
jsonMatches = jsonMatches.data

function top10EconomicalBowlerYear2015() {
  const matchIdSeason = jsonMatches.reduce((accumulator, match) => {
    if (match.season === "2015") {
      accumulator[match.id] = match.season
    }
    return accumulator
  }, {})

  const allMatchesPlayed2015 = jsonDeliveries.filter((element) => {
    if (matchIdSeason[element.match_id]) {
      return true
    }
  })

  const bowlerObject = allMatchesPlayed2015.reduce((accumulator, element) => {
    if (typeof element.bowler === "undefined") {
      return accumulator
    }
    if (!accumulator[element.bowler]) {
      const totalDeliveriesByBowler = allMatchesPlayed2015.reduce(
        (acc, item) => {
          if (element.bowler === item.bowler) {
            return acc + 1
          }
          return acc
        },
        0
      )
      const totalRunsGivenByBowler = allMatchesPlayed2015.reduce(
        (acc, item) => {
          if (element.bowler === item.bowler) {
            return Number(item.total_runs) + acc
          }
          return acc
        },
        0
      )

      accumulator[element.bowler] =
        (totalRunsGivenByBowler / totalDeliveriesByBowler) * 6
    }
    return accumulator
  }, {})

  const bowlerEconomy = Object.keys(bowlerObject).reduce(
    (accumulator, element) => {
      const singleBowlerEconomy = {
        name: element,
        economy: bowlerObject[element]
      }
      accumulator.push(singleBowlerEconomy)
      return accumulator
    },
    []
  )

  bowlerEconomy.sort((bowlerA, bowlerB) => {
    // contains the economy of the bowler stored as a number
    return bowlerA.economy - bowlerB.economy
  })
  const top10EconomicalBowler = bowlerEconomy.slice(0, 10)
  return top10EconomicalBowler
}

module.exports = top10EconomicalBowlerYear2015
