const papa = require("papaparse")
const fs = require("fs")
const path = require("path")

const csvDeliveries = fs.readFileSync(
  path.resolve(__dirname, "../data/deliveries.csv"),
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, { header: true })
jsonDeliveries = jsonDeliveries.data

function bowlerWithBestEconomyInSuperOber() {
  // filter deliveries that deliver in super over
  const superOverArray = jsonDeliveries.filter((element) => {
    return element.is_super_over === "1"
  })

  const bowlerObject = superOverArray.reduce(
    (accumulator, element) => {
      accumulator.deliveriesByBowler[element.bowler] =
        (accumulator.deliveriesByBowler[element.bowler] || 0) + 1
      accumulator.runsGivenByBowler[element.bowler] =
        (accumulator.runsGivenByBowler[element.bowler] || 0) +
        Number(element.total_runs)
      return accumulator
    },
    { deliveriesByBowler: {}, runsGivenByBowler: {} }
  )

  const bowlerEconomy = Object.keys(bowlerObject.deliveriesByBowler).map(
    (element) => {
      // creating an array name bowler Economy and passing an object contain name and economy
      return {
        name: element,
        economy:
          (bowlerObject.runsGivenByBowler[element] /
            bowlerObject.deliveriesByBowler[element]) *
          6,
      }
    }
  )

  bowlerEconomy.sort((firstElement, secondElement) => {
    // contains the economy of the bowler stored as a number
    return firstElement.economy - secondElement.economy
  })
  return bowlerEconomy[0]
}

module.exports = bowlerWithBestEconomyInSuperOber
