const papa = require("papaparse")
const fs = require("fs")
const path = require("path")

const csvMatches = fs.readFileSync(
  path.resolve(__dirname, "../data/matches.csv"),
  "utf-8"
)

let jsonMatches = papa.parse(csvMatches, { header: true })
jsonMatches = jsonMatches.data

function teamWonTossAndMatch() {
  // using reduce function i will check same team won the toss and won the match than I will store in object with team name
  const teams = jsonMatches.reduce((accumulator, element) => {
    if (
      element.toss_winner === element.winner &&
      typeof element.winner !== "undefined"
    ) {
      accumulator[element.winner] = (accumulator[element.winner] || 0) + 1
    }
    return accumulator
  }, {})
  return teams
}

module.exports = teamWonTossAndMatch
